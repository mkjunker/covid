#! /usr/bin/env sh

if [ -z $1 ] ; then LINES=3 ; else LINES=$1 ; fi

for t in 660100 660201 660202 660301 660303 660304 ; do
    ./covid.py -i COVID-19_Historical_Data_by_Census_Tract.csv -oGEOID -oPOSITIVE -oDEATHS -oTEST_NEW -oPOS_NEW -oDTH_NEW -oPOS_WIN -oPOS_PCT_WIN -oBURDEN -G 55089${t} | tail -n$LINES
done
./sum.sh -i COVID-19_Historical_Data_by_Census_Tract.csv -o BURDEN --meq --date | tail -n$LINES
