#!/usr/bin/env python3
'''Time-stamp: <Sep 3, 2021, 2:38 pm, Matthew K. Junker>

Most people will have mild illness and can recover at home without
medical care and may not need to be tested.

https://www.cdc.gov/coronavirus/2019-ncov/symptoms-testing/testing.html
https://web.archive.org/web/20200428231014/https://www.cdc.gov/coronavirus/2019-ncov/symptoms-testing/testing.html

TO DO: The --county option doesn't work with spaces or periods removed!
The format needs to match the CSV file, not the POPULATION
dictionary.

Census tracts: http://gis-mclio.opendata.arcgis.com/datasets/d60028f77427442cac14d8f6effab8b1_1
https://geodata.wisc.edu/catalog/MilwaukeeCounty_d60028f77427442cac14d8f6effab8b1_1
https://geodata.wisc.edu/catalog/DHS_40a25761793c4501a291852b7d39432b_9
https://data.dhsgis.wi.gov/datasets/b913e9591eae4912b33dc5b4e88646c5_10
https://data.dhsgis.wi.gov/datasets/covid-19-data-by-census-tract?geometry=-89.199%2C42.696%2C-85.178%2C43.399
https://geodata.wisc.edu/?utf8=%E2%9C%93&f%5Bdct_provenance_s%5D%5B%5D=U.S.+Geological+Survey&search_field=all_fields&q=Thiensville
https://geodata.wisc.edu/?utf8=%E2%9C%93&f%5Bdct_isPartOf_sm%5D%5B%5D=Aerial+Imagery&search_field=all_fields&q=ozaukee

'''
import argparse
import logging
import sys
import csv
import datetime
import PyGnuplot as gp
import tempfile
import os
# https://stackoverflow.com/questions/9644651/gnuplot-py-error-with-datetime-chart

# County populations
# https://www.census.gov/data/tables/time-series/demo/popest/2010s-counties-total.html

minver = 3, 6
args = None

# https://www.dhs.wisconsin.gov/covid-19/data-101.htm
# Upper limits, inclusive, cases/100000 in past 2 weeks
burdens = dict(low=10, moderate=50, moderately_high=100, high=100000)

SPECIAL = ['Mequon', 'Thiensville']

# July 1, 2019
POPULATION = dict(
    WI=5822434,
    Adams=20220,
    Ashland=15562,
    Barron=45244,
    Bayfield=15036,
    Brown=264542,
    Buffalo=13031,
    Burnett=15414,
    Calumet=50089,
    Chippewa=64658,
    Clark=34774,
    Columbia=57532,
    Crawford=16131,
    Dane=546695,
    Dodge=87839,
    Door=27668,
    Douglas=43150,
    Dunn=45368,
    Eau_Claire=104646,
    Florence=4295,
    Fond_du_Lac=103403,
    Forest=9004,
    Grant=51439,
    Green=36960,
    Green_Lake=18913,
    Iowa=23678,
    Iron=5687,
    Jackson=20643,
    Jefferson=84769,
    Juneau=26687,
    Kenosha=169561,
    Kewaunee=20434,
    La_Crosse=118016,
    Lafayette=16665,
    Langlade=19189,
    Lincoln=27593,
    Manitowoc=78981,
    Marathon=135692,
    Marinette=40350,
    Marquette=15574,
    Menominee=4556,
    Milwaukee=945726,
    Monroe=46253,
    Oconto=37930,
    Oneida=35595,
    Outagamie=187885,
    Ozaukee=89221,
    Pepin=7287,
    Pierce=42754,
    Polk=43783,
    Portage=70772,
    Price=13351,
    Racine=196311,
    Richland=17252,
    Rock=163354,
    Rusk=14178,
    St_Croix=90687,
    Sauk=64442,
    Sawyer=16558,
    Shawano=40899,
    Sheboygan=115340,
    Taylor=20343,
    Trempealeau=29649,
    Vernon=30822,
    Vilas=22195,
    Walworth=103868,
    Washburn=15720,
    Washington=136034,
    Waukesha=404198,
    Waupaca=50990,
    Waushara=24443,
    Winnebago=171907,
    Wood=72999,
    # 2018
    Mequon=24835,
    Thiensville=3147,
    G55089660100=3147,
    G55089660201=4067,
    G55089660202=5897,
    G55089660301=5885,
    G55089660303=5059,
    G55089660304=3160)


NUMERIC_FIELDS = [
    "NEGATIVE",
    "POSITIVE",
    "HOSP_YES",
    "HOSP_NO",
    "HOSP_UNK",
    "POS_FEM",
    "POS_MALE",
    "POS_OTH",
    "POS_0_9",
    "POS_10_19",
    "POS_20_29",
    "POS_30_39",
    "POS_40_49",
    "POS_50_59",
    "POS_60_69",
    "POS_70_79",
    "POS_80_89",
    "POS_90",
    "DEATHS",
    "DTHS_FEM",
    "DTHS_MALE",
    "DTHS_OTH",
    "DTHS_0_9",
    "DTHS_10_19",
    "DTHS_20_29",
    "DTHS_30_39",
    "DTHS_40_49",
    "DTHS_50_59",
    "DTHS_60_69",
    "DTHS_70_79",
    "DTHS_80_89",
    "DTHS_90",
    "IP_Y_0_9",
    "IP_Y_10_19",
    "IP_Y_20_29",
    "IP_Y_30_39",
    "IP_Y_40_49",
    "IP_Y_50_59",
    "IP_Y_60_69",
    "IP_Y_70_79",
    "IP_Y_80_89",
    "IP_Y_90",
    "IP_N_0_9",
    "IP_N_10_19",
    "IP_N_20_29",
    "IP_N_30_39",
    "IP_N_40_49",
    "IP_N_50_59",
    "IP_N_60_69",
    "IP_N_70_79",
    "IP_N_80_89",
    "IP_N_90",
    "IP_U_0_9",
    "IP_U_10_19",
    "IP_U_20_29",
    "IP_U_30_39",
    "IP_U_40_49",
    "IP_U_50_59",
    "IP_U_60_69",
    "IP_U_70_79",
    "IP_U_80_89",
    "IP_U_90",
    "IC_YES",
    "IC_Y_0_9",
    "IC_Y_10_19",
    "IC_Y_20_29",
    "IC_Y_30_39",
    "IC_Y_40_49",
    "IC_Y_50_59",
    "IC_Y_60_69",
    "IC_Y_70_79",
    "IC_Y_80_89",
    "IC_Y_90",
    "POS_AIAN",
    "POS_ASN",
    "POS_BLK",
    "POS_WHT",
    "POS_MLTOTH",
    "POS_UNK",
    "POS_E_HSP",
    "POS_E_NHSP",
    "POS_E_UNK",
    "DTH_AIAN",
    "DTH_ASN",
    "DTH_BLK",
    "DTH_WHT",
    "DTH_MLTOTH",
    "DTH_UNK",
    "DTH_E_HSP",
    "DTH_E_NHSP",
    "DTH_E_UNK",
    "POS_HC_Y",
    "POS_HC_N",
    "POS_HC_UNK",
    "DTH_NEW",
    "POS_NEW",
    "NEG_NEW",
    "TEST_NEW"
]

ages = ['POS_0_9',
        'POS_10_19',
        'POS_20_29',
        'POS_30_39',
        'POS_40_49',
        'POS_50_59',
        'POS_60_69',
        'POS_70_79',
        'POS_80_89',
        'POS_90']

def fix_name(name):
    return name.replace(' ', '_').replace('.', '')


def restore_name(name):
    n = name.replace('_', ' ')
    n = n.replace('St ', 'St. ')
    return n.title()


def parse_args(argv):
    '''Parse arguments, set up logging.'''
    global args

    parser = argparse.ArgumentParser(
        description='Slices and dices COVID-19 data.',
        epilog=('Synthesized outputs: TEST_WIN, POS_WIN, NEG_WIN, '
                'DTH_WIN, POS_PCT_WIN, TEST_TOT, HOSP_WIN, HC_WIN.  '
                'POS_NEW, NEG_NEW, and DTH_NEW may be calculated.'))

    verbose_help = '-hh' in argv

    # See argparse.FileType

    parser.add_argument('-hh', action='help',
                        help="show verbose help and exit")
    parser.add_argument('--input', '-i',
                        type=str,
                        default='COVID-19_Historical_Data_Table.csv',
                        help='[%(default)s]')
    parser.add_argument('--output', '-o',
                        type=str,
                        action='append')
    parser.add_argument('--graph', '-g',
                        action='store_true')
    parser.add_argument('--date-squash', '-d',
                        action='store_true')
    parser.add_argument('--list-fields', '-l',
                        action='store_true')
    parser.add_argument(
        '--ages-positive', '-ap',
        action='store_true',
        help='Equivalent to -o POS_... for the age ranges.')
    parser.add_argument(
        '--ages-deaths', '-ad',
        action='store_true',
        help='Equivalent to -o DTH_... for the age ranges.')
    parser.add_argument(
        '--ages-deaths-window', '-wad',
        action='store_true',
        help='Equivalent to -o DTH_... for the age ranges.')
    parser.add_argument(
        '--ages-hospital', '-ah',
        action='store_true',
        help='Equivalent to -o IP_Y_... for the age ranges.')
    parser.add_argument(
        '--ages-no-hospital', '-anh',
        action='store_true',
        help='Equivalent to -o IP_N_... for the age ranges.')
    parser.add_argument(
        '--ages-icu', '-ai',
        action='store_true',
        help='Equivalent to -o IC_Y_... for the age ranges.')
    parser.add_argument(
        '--Age', '-A',
        type=int,
        choices=[0,1,2,3,4,5,6,7,8,9],
        help='DTHS, IP_Y, etc for designated range (decade).')
    parser.add_argument(
        '--filter', '-f',
        type=str,
        default="x['GEO']=='State'",
        help='predicate [%(default)s]')
    parser.add_argument(
        '--window', '-w',
        type=int,
        metavar='DAYS',
        default=7,
        help='average window [%(default)s]')
    parser.add_argument('--name', '-n',
                        type=str,
                        help="Same as -f \"x['NAME'] == name\"")
    parser.add_argument('--geoid', '-G',
                        type=str,
                        help=("Same as -f \"x.get('GEOID','')."
                              "startswith('geoid')\""))
    parser.add_argument('--exactid', '-e',
                        type=str,
                        help=("Same as -f \"x['GEOID'] == 'geoid'\""))
    parser.add_argument('--thiensville',
                        action='store_true',
                        help="Same as -f \"x['GEOID'] == '55089660100'\"")
    parser.add_argument('--thiensville2',
                        action='store_true',
                        help="Same as -f \"x['NAME'] == 'Thiensville village'\"")
    parser.add_argument('--mequon2',
                        action='store_true',
                        help="Same as -f \"x['NAME'] == 'Mequon city'\"")
    parser.add_argument('--mequon',
                        action='store_true',
                        help="Same as -f...")
    parser.add_argument('--mtsd',
                        action='store_true',
                        help="Same as -f \"x['NAME'] == 'Mequon-Thiensville'\"")
    parser.add_argument('--counties', '-C',
                        action='store_true',
                        help="Same as -w1 -f\"x['GEO']=='County'\" -o NAME")
    for c in POPULATION:
        if c in SPECIAL:
            continue
        parser.add_argument(
            '--' + fix_name(c),
            action='store_true',
            help="Same as -f \"x['NAME'] == '{}'\"".format(
                restore_name(c))
            if verbose_help else argparse.SUPPRESS)
    parser.add_argument('-hm',
                        action='store_true',
                        required=False,
                        help=('Module help' if verbose_help
                              else argparse.SUPPRESS))
    loglevels = list(logging._levelToName.values())
    loglevels.remove('NOTSET')
    loglevels = list(map(str.lower, loglevels))
    parser.add_argument('--log',
                        required=False,
                        metavar='LEVEL',
                        choices=loglevels,
                        help=('set logging level: ' + str(loglevels)
                              + ' [%(default)s]' if verbose_help
                              else argparse.SUPPRESS),
                        default='warning')
    parser.add_argument(
        '--log-file', '-lf',
        required=False,
        nargs='?',
        const=(
            os.path.join(
                tempfile.gettempdir(),
                os.path.splitext(os.path.basename(__file__))[0])
            + '.log'),
        help=(
            'Log file name, in addition to stderr '
            '[%(const)s if no value given]'
            if verbose_help
            else argparse.SUPPRESS),
        metavar='PATH')
    parser.add_argument(
        '--log-no-console',
        action='store_true',
        help=('do not log to console '
              '(only effective if --log-file is specified)'
              if verbose_help
              else argparse.SUPPRESS))

    args = parser.parse_args(args=argv)
    level = getattr(logging, args.log.upper())
    handlers = ([] if args.log_no_console
                else [logging.StreamHandler(sys.stderr)])
    if args.log_file is not None:
        handlers.append(logging.FileHandler(args.log_file))
    logging.basicConfig(level=level,
                        handlers=handlers)
    if args.hm:
        help(__name__)
        sys.exit(0)

    for c in POPULATION:
        if c in SPECIAL:
            continue
        if getattr(args, c):
            args.filter = "x['NAME'] == '{}'".format(restore_name(c))
    if args.mequon:
        args.filter = "x.get('GEOID','').startswith('55089660')"
    if args.exactid is not None:
        args.filter = "x['GEOID'] == '{}'".format(args.exactid)
    if args.geoid is not None:
        args.filter = "x.get('GEOID','').startswith('{}')".format(
            args.geoid)
    if args.name is not None:
        args.filter = "x['NAME'] == args.name"
#    if args.ozaukee:
#        args.filter = "x['NAME'] == 'Ozaukee'"
#    if args.milwaukee:
#        args.filter = "x['NAME'] == 'Milwaukee'"
#    if args.dane:
#        args.filter = "x['NAME'] == 'Dane'"
    if args.thiensville:
        args.filter = "x['GEOID'] == '55089660100'"
    if args.thiensville2:
        args.filter = "x['NAME'] == 'Thiensville village'"
    if args.mequon2:
        args.filter = "x['NAME'] == 'Mequon city'"
    if args.mtsd:
        args.filter = "x['NAME'] == 'Mequon-Thiensville'"
    if args.counties:
        args.filter = "x['GEO']=='County'"
        args.window = 1
        args.output.append('NAME')
        # These are calculated incorrectly in this environment.
        args.output.remove('POS_WIN')
        # args.output.remove('POS_PCT_WIN')
    if args.output is None:
        args.output = []
    if args.ages_positive:
        args.output.extend(ages)
    if args.ages_deaths:
        args.output.extend([a.replace('POS', 'DTHS') for a in ages])
    if args.ages_hospital:
        args.output.extend([a.replace('POS', 'IP_Y') for a in ages])
    if args.ages_no_hospital:
        args.output.extend([a.replace('POS', 'IP_N') for a in ages])
    if args.ages_icu:
        args.output.extend([a.replace('POS', 'IC_Y') for a in ages])
    if args.ages_deaths_window:
        args.output.extend([a.replace('POS', 'WDTHS') for a in ages])
    if args.Age is not None:
        outs = ['POS', 'DTHS', 'IP_Y', 'IP_N', 'IC_Y']
        template = ages[args.Age]
        args.output.extend([template.replace('POS', f)
                            for f in outs])


def checkminver():
    '''Insure the needed python version is used.

If an insufficient version is used, exit with code 1.'''
    if sys.version_info < minver:
        logging.critical(
            'Minimum python version is '+'.'.join(map(str, minver)))
        logging.critical(
            'Using '+'.'.join(map(str, sys.version_info)))
        sys.exit(1)


def get_pop(line):
    modName = fix_name(line['NAME'])
    if modName in POPULATION:
        return POPULATION[modName]
    elif args.mequon or args.mtsd:
        return POPULATION['Mequon'] + POPULATION['Thiensville']
    elif args.thiensville or args.thiensville2:
        return POPULATION['Thiensville']
    elif args.mequon or args.mequon2:
        return POPULATION['Mequon']
    elif 'G' + line['GEOID'] in POPULATION:
        return POPULATION['G' + line['GEOID']]
    return None


def squash(inn):
    out = []
    d = []
    for i in inn:
        if len(d) == 0 or i['datetime'].date() != d[-1]['datetime'].date():
            for f in NUMERIC_FIELDS:
                i[f] = int(i.get(f) or 0)
            d.append(i)
            continue
        for f in NUMERIC_FIELDS:
            if int(i.get(f) or 0) == -999:
                continue
            if d[-1][f] == -999:
                d[-1][f] = 0
            d[-1][f] += int(i.get(f) or 0)
    return d


def diff(then, now, field):
    '''Return the difference in FIELD between THEN and NOW.
THEN should be one day *before* the window of interest.
This function should be used where sums cannot be calculated.'''
    t = max(int(then.get(field) or 0), 0)
    n = max(int(now.get(field) or 0), 0)
    return n - t


def new(prev, now, dest, source):
    '''Populated a _NEW field that is easy to calculate.'''
    if not now[dest]:
        now[dest] = (
            max(int(now[source] or 0), 0)
            - max(int(prev[source] or 0), 0))


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parse_args(argv)
    checkminver()

    data = list()

#    bytes = min(32, os.path.getsize(args.input))
#    raw = open(args.input, 'rb').read(bytes)
#    result = chardet.detect(raw)
#    encoding = result['encoding']
#    with codecs.open(args.input, encoding=codecs.utf_8) as f:
# https://stackoverflow.com/questions/13590749/reading-unicode-file-data-with-bom-chars-in-python
    with open(args.input, encoding='utf-8-sig', newline='') as f:
        reader = csv.DictReader(f)
        for row in reader:
            logging.debug(row)
            row['datetime'] = datetime.datetime.strptime(
                row['DATE'], '%Y/%m/%d %H:%M:%S+00')
            # .astimezone(datetime.timezone.utc)
            data.append(row)

    if args.list_fields:
        for f in data[0]:
            print(f)
        return 0
    data = data[1:]
    filtered = [x for x in data if eval(args.filter)]
    filtered.sort(key=lambda d: d['datetime'])

    # Synthesize _NEW values if necessary, BEFORE squashing.
    for n, x in enumerate(filtered):
        prev = filtered[max(n-1, 0)]

        new(prev, x, 'POS_NEW', 'POSITIVE')
        new(prev, x, 'NEG_NEW', 'NEGATIVE')
        new(prev, x, 'DTH_NEW', 'DEATHS')

        if x['TEST_NEW'] == '':
            x['TEST_NEW'] = (int(x['POS_NEW'] or 0)
                             + int(x['NEG_NEW'] or 0))

    if args.date_squash:
        filtered = squash(filtered)

    # windowed summaries
    tests = 0
    for n, x in enumerate(filtered):
        window = filtered[max(n-args.window+1, 0):n+1]
        before_window = filtered[max(n-args.window, 0)]
        before_burden = filtered[max(n-args.window, 0)]
        n = max(n, 0)
        prev = filtered[max(n-1, 0)]

        x['TEST_WIN'] = sum([int(x['TEST_NEW'] or 0) for x in window])

        tests += max(int(x['TEST_NEW'] or 0), 0)

        x['DTH_WIN'] = sum([int(x['DTH_NEW'] or 0) for x in window])
        x['POS_WIN'] = diff(before_window, x, 'POSITIVE')
        x['NEG_WIN'] = diff(before_window, x, 'NEGATIVE')
        x['HOSP_WIN'] = diff(before_window, x, 'HOSP_YES')
        x['HC_WIN'] = diff(before_window, x, 'POS_HC_Y')
        for a in ages:
            daa = a.replace('POS', 'DTHS')
            waa = f'W{daa}'
            if '01/29' in x['DATE']:
                pass
#                logging.debug(x)
#                breakpoint()
            x[waa] = diff(before_window, x, daa)

        try:
            pop = get_pop(filtered[-1])
            x['BURDEN'] = diff(
                before_burden, x, 'POSITIVE') / pop * 100000
        except:
            x['BURDEN'] = -999
        if args.window == 1:
            try:
                x['POS_PCT_WIN'] = float(
                    x['POS_NEW']) / float(x['TEST_NEW'])
            except:
                x['POS_PCT_WIN'] = -999
        else:
            x['POS_PCT_WIN'] = -999 if x['TEST_WIN'] == 0 else (
                x['POS_WIN'] / x['TEST_WIN'])
        x['TEST_TOT'] = tests

    if args.graph:
        def old_graph():
            #    gp.c('set xdata time')
            #    gp.c('set timefmt "%Y-%m-%d %H:%M:%S"')
            state_data = filtered  # [x for x in data if eval(args.filter)]
            dates = [x['datetime'].date().strftime('%Y-%m-%d')
                     for x in state_data]
            #    dates = range(len(dates))
            new_tests = [int(x['TEST_NEW'] or 0) for x in state_data]
            new_tests_ma = [sum(new_tests[max(x-args.window, 0):x])/args.window
                            for x in list(range(len(new_tests)))]
            frac_pos = [float(x['POS_NEW'] or 0)
                        / max(float(x['TEST_NEW'] or 1), 1.0) for x in state_data]
            frac_ma = [sum(frac_pos[max(x-args.window, 0):x])/args.window
                       for x in list(range(len(frac_pos)))]
            gp.s([dates, new_tests, frac_pos, frac_ma, new_tests_ma])
            gp.c('set xdata time')
            gp.c('set timefmt "%Y-%m-%d"')
            gp.c('set format x "%m/%d"')
            gp.c('set y2range [0:0.15]')
            gp.c('set y2tics 0.01')
            gp.c('set ytics nomirror')
            gp.c('set xtics rotate')
            gp.c(
                'plot "tmp.dat" using 1:2 title "new tests" '
                + 'with lines lw 2 axis x1y1, '
                + '"tmp.dat" using 1:5 title "new tests MA '
                + str(args.window) + '" '
                + 'with lines axis x1y1, '
                + '"tmp.dat" using 1:3 title "Frac. Pos" with '
                + 'lines lw 2 axis x1y2, '
                + '"tmp.dat" using 1:4 title "Frac. Pos MA" with '
                + 'lines axis x1y2')
            return 0
        def new_graph():
            state_data = filtered  # [x for x in data if eval(args.filter)]
            dates = [x['datetime'].date().strftime('%Y-%m-%d')
                     for x in state_data]
            burden = [x['BURDEN'] for x in state_data]
            gp.s([dates, burden])
            gp.c('set xdata time')
            gp.c('set timefmt "%Y-%m-%d"')
            gp.c('set format x "%m/%d/%y"')
            # gp.c('set ytics 1, 5, 5000 logscale')
            # gp.c('set yrange [1:5000]')
            # gp.c('set log y')
            # gp.c('set ytics nomirror')
            gp.c('set ytics 50')
            gp.c('set xtics rotate')
            gp.c('set terminal dumb size 180 40')
            gp.c('set terminal x11')
            gp.c(
                'plot "tmp.dat" using 1:2 title "Burden" '
                + 'with lines lw 2 axis x1y1')
            return 0
        return new_graph()

    for d in filtered:
        print('{:3} {:%a %Y-%m-%d}'.format(
            (datetime.date.today() - d['datetime'].date()).days,
            d['datetime']), end='')
        for o in args.output:
            if isinstance(d.get(o, None), float):
                s = f'{d[o]:.3f}'
            else:
                s = str(d.get(o, ''))
            ss = f'{o}:{s}'
            print(f' {ss:14}', end='')
        print('')
    try:
        pop = get_pop(filtered[-1])
        burden = diff(filtered[-args.window-1], filtered[-1],
                      'POSITIVE') / pop * 100000
        print(f'Window: {args.window}  Burden: {burden:.2f}'
              f'  Population: {pop:,d}')
    except:
        pass
    return 0


if __name__ == "__main__":
    sys.exit(main())
