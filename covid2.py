#!/usr/bin/env python3
'''Time-stamp: <Feb 9, 2022, 9:48 am, Matthew K. Junker>

Cached data are stored in data.cache.  Delete that file when new data
are downloaded.  Deletion is done automatically when un.sh is executed.

'''
import argparse
import logging
import sys
import csv
import datetime
import PyGnuplot as gp
import os
import tempfile
import site
import math
import shelve

minver = 3, 8
args = None


where = dict(
    mtsd =
    dict(population=27250,
         name='Mequon-Thiensville',
         filename='COVID19-Historical-V2-SCHLDIST.csv'),
    thiensville =
    dict(population=3157,
         name='Thiensville village',
         filename='COVID19-Historical-V2-CNTYSUB.csv'),
    ozaukee =
    dict(population=89221,
         name='Ozaukee',
         filename='COVID19-Historical-V2-CNTY.csv'),
    wisconsin =
    dict(population=5890000,
         name='WI',
         filename='COVID19-Historical-V2-ST.csv'))


def parse_args(argv):
    '''Parse arguments, set up logging.'''
    global args

    parser = argparse.ArgumentParser(
        description='')

    verbose_help = '-hh' in argv

    # See argparse.FileType

    parser.add_argument('--input', '-i',
                        type=str,
                        default='COVID19-Historical-V2-SCHLDIST.csv',
                        help='[%(default)s]')
    parser.add_argument('--graph', '-g',
                        type=str,
                        nargs='?',
                        const='POS_7DAYAVG_CP',
                        help='[%(const)s]')
    parser.add_argument('--logarithmic', '-L',
                        action='store_true')
    parser.add_argument('--title', '-T', type=str)
    parser.add_argument(
        '--filter', '-f',
        type=str,
        default="True",
        help='predicate [%(default)s]')
    for w in where:
        parser.add_argument(
            f'--{w}',
            action='store_true',
            help=f'filter: GEOName {where[w]["name"]}')
    parser.add_argument('--output', '-o',
                        type=str,
                        action='append')
    parser.add_argument('--population', '-p',
                        type=int)
    parser.add_argument('--list-fields',
                        action='store_true')
    parser.add_argument('-hh', action='help',
                        help="show verbose help and exit")
    parser.add_argument('-hm',
                        action='store_true',
                        required=False,
                        help=('Module help' if verbose_help
                              else argparse.SUPPRESS))
    parser.add_argument('--version',
                        action='store_true',
                        help="show version information and exit")
    loglevels = list(logging._levelToName.values())
    loglevels.remove('NOTSET')
    loglevels = list(map(str.lower, loglevels))
    parser.add_argument('--log', '-l',
                        required=False,
                        metavar='LEVEL',
                        choices=loglevels,
                        help=('set logging level: ' + str(loglevels)
                              + ' [%(default)s]' if verbose_help
                              else argparse.SUPPRESS),
                        default='warning')
    parser.add_argument(
        '--log-file', '-lf',
        required=False,
        nargs='?',
        const=(
            os.path.join(
                tempfile.gettempdir(),
                os.path.splitext(os.path.basename(__file__))[0])
            + '.log'),
        help=(
            'Log file name, in addition to stderr '
            '[%(const)s if no value given]'
            if verbose_help
            else argparse.SUPPRESS),
        metavar='PATH')
    parser.add_argument(
        '--log-no-console',
        action='store_true',
        help=('do not log to console '
              '(only effective if --log-file is specified)'
              if verbose_help
              else argparse.SUPPRESS))

    args = parser.parse_args(args=argv)
    level = getattr(logging, args.log.upper())
    handlers = ([] if args.log_no_console
                else [logging.StreamHandler(sys.stderr)])
    if args.log_file is not None:
        handlers.append(logging.FileHandler(args.log_file))
    logging.basicConfig(level=level,
                        handlers=handlers)
    if args.hm:
        help(__name__)
        sys.exit(0)

    for w in where:
        # alternate method: vars(args)[w]
        if getattr(args, w):
            args.filter = "x['GEOName'] == '" + where[w]['name'] + "'"
            args.input = where[w]['filename']
            args.population = where[w]['population']

    if args.version:
        for line in __doc__.splitlines():
            if line.startswith('Time-stamp:'):
                print(line)
        print(f'{__file__=}')
        print(f'{__name__=}')
        print(f'{sys.executable=}')
        print('sys.version={}.{}'.format(*sys.version_info))
        print(f'{__package__=}')
        print(f'{site.PREFIXES=}')
        print(f'{site.USER_SITE=}')
        print(f'{site.USER_BASE=}')
        sys.exit(0)

    return args


def checkminver():
    '''Insure the needed python version is used.

If an insufficient version is used, exit with code 1.'''
    if sys.version_info < minver:
        logging.critical(
            'Minimum python version is '+'.'.join(map(str, minver)))
        logging.critical(
            'Using '+'.'.join(map(str, sys.version_info)))
        sys.exit(1)


def graph(filtered):
    state_data = filtered  # [x for x in data if eval(args.filter)]
    dates = [x['datetime'].date().strftime('%Y-%m-%d')
             for x in state_data]
    try:
        multiplier = (7.0 / args.population * 100000.0
                      if args.graph == 'POS_7DAYAVG_CP' else 1.0)
    except TypeError:
        multiplier = 1.0
    if args.logarithmic:
        burden = [max(1.0, float(x[args.graph]) * multiplier)
                  for x in state_data]
    else:
        burden = [max(float(x[args.graph]) * multiplier, 0)
                  for x in state_data]
    gp.s([dates, burden])
    gp.c('set xdata time')
    gp.c('set timefmt "%Y-%m-%d"')
    gp.c('set format x "%m/%d/%y"')
    # gp.c('set ytics 1, 5, 5000 logscale')
    # gp.c('set yrange [1:5000]')
    if args.logarithmic:
        gp.c('set logscale y')
    # gp.c('set ytics nomirror')
    ytics = round(max(burden) / 10, -int(math.log10(max(burden))))
    if ytics < 1:
        ytics = round(max(burden) / 10,
                      -int(math.log10(max(burden))) + 1)
    if args.filter == 'True':
        title = args.title or f'{args.input}'
    else:
        title = args.title or f'{args.input}\\n{args.filter}'
    gp.c(f'set title "{title}"')
    if args.logarithmic:
        pass
    else:
        gp.c(f'set ytics {ytics}')
    gp.c('set xtics rotate')
    gp.c('set terminal dumb size 180 40')
    gp.c('set terminal x11')
    legend = args.graph.replace('_', r'\\_')
    gp.c(
        f'plot "tmp.dat" using 1:2 title '
        f'"{legend} {multiplier:.3f}" '
        'with lines lw 2 axis x1y1')
    return 0

def main(argv=None):
    '''Main function.'''
    if argv is None:
        argv = sys.argv[1:]

    parse_args(argv)
    checkminver()

    with shelve.open(f'{args.input}.cache') as db:
        if args.filter in db:
            logging.debug('Used cache')
            filtered = db[args.filter]
        else:
            logging.debug('No cache')
            data = list()
            with open(args.input, encoding='utf-8-sig', newline='') as f:
                reader = csv.DictReader(f)
                for row in reader:
                    logging.debug(row)
                    row['datetime'] = datetime.datetime.strptime(
                        row['Date'], '%m/%d/%Y')
                    # .astimezone(datetime.timezone.utc)
                    data.append(row)
            filtered = [x for x in data[1:] if eval(args.filter)]
            filtered.sort(key=lambda d: d['datetime'])
            db[args.filter] = filtered

    if args.list_fields:
        with open(args.input, encoding='utf-8-sig', newline='') as f:
            fields = f.readline().split(',')
            for f in fields:
                print(f)
        return 0

    if args.graph:
        graph(filtered)
    if args.output is None:
        args.output = [args.graph]
    for d in filtered:
        print('{:3} {:%a %Y-%m-%d}'.format(
            (datetime.date.today() - d['datetime'].date()).days,
            d['datetime']), end='')
        for o in args.output:
            if isinstance(d.get(o, None), float):
                s = f'{d[o]:.3f}'
            else:
                s = str(d.get(o, ''))
            ss = f'{o}:{s}'
            print(f' {ss:14}', end='')
        print('')


if __name__ == "__main__":
    sys.exit(main())
